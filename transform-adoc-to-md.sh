#!/bin/bash

help() {
    echo "Usage: $(basename $0) [-iohvV]" 2>&1
    echo "Transform asciidoc files (.adoc) into markdown (.md) with keep in similar folder and files structure"
    echo
    echo "Syntax: $(basename $0) <source folder> (optional) <output folder> [i|o|h|v|V]"
    echo "options:"
    echo "h     Print this Help."
    echo "i     Specify input folder, default is '.'. "
    echo "o     Specify output folder, default is '../exported-markdown-notes'"
    echo "v     Check version of requirements."
    echo "V     Print software version and exit."
    echo
}

# Tranform .adoc file into .md file using pandoc and asciidoctor
transform_one_file() {
    FILE=$1
    file_out=${FILE%.adoc}
    output=".md"
    output_filename=$file_out$output

    asciidoctor -b docbook -a leveloffset=+1 -o - $FILE | pandoc --atx-headers --wrap=preserve -t markdown_strict -f docbook - >$output_filename
}

transform_folder() {
    # Copy folder structure and copy file
    # rsync -a -f"+ */" -f"+ *.adoc" -f"- *" $1 $1/markdown-notes/
    echo "input folder = ${input_folder}"
    echo "output folder = ${output_folder}"

    cp -aR $input_folder $output_folder

    cd $output_folder

    FILES=$(find . -type f -name '*.adoc')
    for f in $FILES; do
        echo $f
        transform_one_file $f
        rm -rf $f
    done
}

input_folder=.
output_folder=../exported-markdown-notes/

if [[ ${#} -eq 0 ]]; then
    transform_folder
    exit 0
fi

while getopts ":i:o:h" opt; do
    case $opt in
    i)
        input_folder="$OPTARG"
        ;;
    o)
        output_folder="$OPTARG"
        ;;
    h)
        help
        exit 0
        ;;
    ?)
        echo "Invalid option: -${OPTARG}."
        help
        exit 0
        ;;
    esac
done
shift $((OPTIND - 1))

transform_folder
