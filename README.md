# asciidoc-to-markdown

Simple bash script for transforming asciidoc files into markdown (downgrade, but necessary for some tools) 

## Usage

    usage: transform-adoc-to-md.sh [-iohvV]
    Transform asciidoc files (.adoc) into markdown (.md) with keep in similar folder and files structure

    Syntax: transform-adoc-to-md.sh <source folder> (optional) <output folder> [i|o|h|v|V]
    options:
    h     Print this Help.
    i     Specify input folder, default is '.'.
    o     Specify output folder, default is '../exported-markdown-notes'
    v     Check version of requirements.
    V     Print software version and exit.
