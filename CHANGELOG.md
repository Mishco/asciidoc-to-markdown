# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- version check 
- installation requirements

## [1.0.0] - 2021-02-25
### Added
- Working version of script
- input/output folder can be specified 
